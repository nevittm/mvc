<?php

	class AccountModel extends Model {
		private $db;

		public function __construct() {
			$this->db = Database::getInstance()->getDB();
		}

		public function query() {
		}

		public function getAll() {
			

			$q = $this->db->query("SELECT * FROM membership");

			$list = array();

			while($row = $q->fetch_assoc()) {
				$list['names']["id"][] = $row['id'];
				$list['names']["first"][] = $row['first'];
				$list['names']["last"][] = $row['last'];
			}

			return $list;
		}
	}