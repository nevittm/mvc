<?php
			class FrontController {
				private $controller;
				private $action;
				private $param;

				public function __construct() {	
					$url = isset($_GET['url']) ? $_GET['url'] : '';		
				
					if(empty($url)) {
						$url = "account/index";
					}

					$separate = explode('/',$url);
					$query = array();					

					foreach($separate as $item) {
						$query[] = stripslashes($item);										
					}

					$this->controller = !empty($query[0]) ? $query[0] : 'default';
					$this->action = !empty($query[1]) ? $query[1] : 'index';
					
					if(count($query) > 2) {
						for($i = 2; $i < count($query); $i++) {
							$this->param[] = $query[$i];
						}
					}

					$this->runController();	
				}

				private function runController() {					
					$c = ucfirst($this->controller)."Controller";
					$fileFound = false;
					if(file_exists("controllers/".strtolower($c).".php")) {
						include strtolower("controllers/".$c.".php");						
						$fileFound = true;
					}
				
					if($fileFound && class_exists($c)) {					 	
							$ctrl = new $c($this->controller);
							if(method_exists($ctrl,$this->action)) {			
								if(!empty($this->param)) {
									$ctrl->{$this->action}($this->param);
								}
								else {									
									$ctrl->{$this->action}();
								}
							}
							else {
								echo "Method does not exist";
							}
					}
					else {
						echo "Class not found";
					}
																			
				}
			}
?>