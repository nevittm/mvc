<?php
	class TestController extends Controller {

		public function __construct($controllerName) {
			parent::__construct($controllerName);
		}

		public function index() {
			$this->render("testview.tpl");
		}
	}
?>