<?php

	class AccountController extends Controller {

		public function __construct($controllerName) {
			parent::__construct($controllerName);
		}

		public function index() {
			$this->getView()->render("account/default.tpl");
		}

		public function loginform() {
			$this->getView()->render("account/loginformview.tpl");
		}

		public function authenticate() {

			$email = isset($_POST['email']) ? $_POST['email'] : '';
			$password = isset($_POST['password']) ? $_POST['password'] : '';

			$this->getView()->set(array("email" => $email, 
										"password" => $password));
			$this->getView()->render("account/resultlogin.tpl");
		}

		public function viewAll() {
			$model = $this->getModel()->getAll();

			$this->getView()->set(array("names" => $model["names"]));
			$this->getView()->render("defaultview.tpl");
		}

		public function view($param = null) {
			//foreach($param as $p)
		}
	}
?>