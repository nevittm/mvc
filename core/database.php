<?php
	class Database {
		private $db;
		private static $instance;

		private function __construct() {
			$this->db = new mysqli("localhost","root","","social");
		}

		public static function getInstance() {
			return !isset(static::$instance) ? new Static() : static::$instance;
		}

		public function getDB() {
			return $this->db;
		}
	}
?>