<?php
	class View {
		private $params;

		public function set($param) {
			$this->params[] = $param;
		}

		public function render($fileName) {
			include "./views/templates/header.php";

			ob_start();

			if(count($this->params) > 0) { 
				foreach($this->params as $values) {
					extract($values);
				}
			}
			
			include "./views/templates/".$fileName;
			
			echo ob_get_clean();
			
			include "./views/templates/footer.php";
		}
	}