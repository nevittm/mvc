<?php

define("encryption_key", "thisisthebestencryptionclassintheworld1234567890");

class Encryption
{
  // encrypts the string
  public function encryptString($string)
  {
    //if there is no salt, generate one
    if(!encryption_key) {
      echo "Please enter a valid salt key in encryption_key variable";
    }
    return trim(base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, encryption_key, $string, 
    	   MCRYPT_MODE_ECB,mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB), MCRYPT_RAND))));
  }

  // decrypt the string
  public function decryptString($string) {
    return trim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, encryption_key, 
    	   base64_decode($string), MCRYPT_MODE_ECB, mcrypt_create_iv(
    	   mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB), MCRYPT_RAND)));
  }
}