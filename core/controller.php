<?php
	class Controller {
		private $model;
		private $view;

		public function __construct($controllerName) {
			$modelName = $controllerName."Model";
			include './models/'.strtolower($modelName).'.php';
			$this->view = new View();
			$this->model = new $modelName();		
		}

		public function getModel() {
			return $this->model;
		}

		public function getView() {
			return $this->view;
		}

		public function render($fileName) {
			$this->view->render($fileName);
		}
	}